package analizador;

import jflex.exceptions.SilentExit;

public class ExecuteJFlex {

    public static void main(String omega[]) {
        String lexerFile = System.getProperty("user.dir") + "/src/analizador/Lexer.flex",
                lexerFileColor = System.getProperty("user.dir") + "/src/analizador/LexerColor.flex";
        try {
            jflex.Main.generate(new String[]{lexerFile, lexerFileColor});
        } catch (SilentExit ex) {
            System.out.println("Error al compilar/generar el archivo flex: " + ex);
        }
    }
}
